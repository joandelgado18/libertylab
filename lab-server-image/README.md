
# Imagen docker de Websphere Liberty

Imaben base y de aplicación de websphere liberty que se utilizará para construir el laboratorio.


## Ejecución Local

Clonar el proyecto principal

```bash
  # git clone https://github.com/jdelgado18/laboratorio.git
```

Ir al directorio del sub proyecto

```bash
  # cd laboratorio/lab-server-image
```

Construir la imagen

```bash
  # docker build -t lab-server .
```

Iniciar un contenedor

```bash
  # docker run -p 9080:9080 -p 9443:9443 -e CLONE_ID=server1 --name server1 lab-server
```

  
## Tecnología usada para la aplicación

**Server:** JSP, Servlets, EJB, JPA

  
## Proyectos Relacionados

Para que la aplicación dentro del contenedor funcione correctamente se debe complementar con las siguientes imagenes
- lab-ldap
- ibmcom/db2
- ibmcom/mq
  