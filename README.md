
# Laboratorio

Este proyecto construye imágenes y luego las ejecuta para armar un laboratorio donde se prueba la interacción de liberty profile con db2 y mq. Al mismo tiempo se busca probar la funcionalidad del sesioncache de liberty en contenedores.


## Pre requisitos

Es necesario tener instalado el plugin docker-compose, los siguientes pasos son para Linux, si necesita instalarlo en otra plataforma por favor revise la [documentación](https://docs.docker.com/compose/install).

```bash
  sudo yum install -y docker-compose-plugin
```
    
## Ejecución del proyecto

Clonar el proyecto

```bash
  git clone https://joandelgado18@bitbucket.org/joandelgado18/libertylab.git
```

Ir al directorio del proyecto

```bash
  cd libertylab
```

Construir las imágenes

```bash
  docker compose build
```

Iniciar el laboratorio

```bash
  docker compose up -d
```

Ingresar a la página principal del laboratorio con la url www.laboratorio.com