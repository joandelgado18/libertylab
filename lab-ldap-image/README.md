
# Imagen LDAP

Crea una imagen de un contenedor java ldap ligero que simula un Microsoft AD.

## Construir la imagen

Para construir la imagen primero tienes que posicionarte en la carpeta donde está el archivo **Dockerfile** y luego ejecutar el siguiente comando:

```bash
  docker build -t ldap-server .
```

  
## Ejecución del contenedor

Para correr el contenedor se debe ejecutar el siguiente comando:

```bash
  docker run --name ldap-server -p 389:389 -p 636:636 ldap-server
```

  