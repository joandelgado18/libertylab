# Changelog

Todos los cambios importantes serán registrados en este archivo.

El formato está basado en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
y este proyecto se adiere a [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.0.0] - 20/09/2021
### Agregado
- Primer commit del laboratorio a bitbucket

## [1.1.0] - 20/09/2021
### Agregado
- Se configura https a la imagen de nginx