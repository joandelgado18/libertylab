package com.laboratorio.mdb;

import java.sql.Timestamp;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.laboratorio.entity.Laboratorio;

/**
 * Message-Driven Bean implementation class for: QueueMDB
 */
@MessageDriven
public class QueueMDB implements MessageListener {

	@PersistenceContext
	private EntityManager em;
	
    /**
     * Default constructor. 
     */
    public QueueMDB() {
        
    }
	
	/**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
    	try {
			String mensaje = ((TextMessage) message).getText();
			Laboratorio lab = new Laboratorio();
			lab.setMensaje(mensaje);
			lab.setUsuario("no_user");
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			lab.setFecha(timestamp);
			em.persist(lab);
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
