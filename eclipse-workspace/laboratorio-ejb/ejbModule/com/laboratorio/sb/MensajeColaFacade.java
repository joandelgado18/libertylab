package com.laboratorio.sb;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

/**
 * Session Bean implementation class MensajeColaFacade
 */
@Stateless
@LocalBean
public class MensajeColaFacade {

	InitialContext ctx;
	ConnectionFactory cf;
	Connection connection;
	
	/**
	 * Default constructor.
	 */
	public MensajeColaFacade() throws Exception {
		 ctx = new InitialContext();
		 cf = (ConnectionFactory) ctx.lookup("jms/qmlaboratorio");
	}

	public void escribirMensajeCola(String mensaje) throws Exception {
		System.out.println("[LOG] Colocando mensaje en la cola");
		connection = cf.createConnection();
		connection.start();
		Queue inQueue = (Queue) ctx.lookup("jms/qlaboratorio");
		boolean transacted = false;
		Session session = connection.createSession(transacted, Session.AUTO_ACKNOWLEDGE);
		MessageProducer mp = session.createProducer(inQueue);
		TextMessage message = session.createTextMessage(mensaje);
		mp.send(message);
	}

}
