package com.laboratorio.sb;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.laboratorio.entity.Laboratorio;

/**
 * Session Bean implementation class DBFacade
 */
@Dependent
@Stateless
@LocalBean
public class DBFacade {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public DBFacade() {
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	public List<Laboratorio> listarMensajesLaboratorio() {
		System.out.println("[LOG] Listando mensajes de base de datos.");
		Query q = em.createQuery("SELECT l FROM Laboratorio L");
		return q.getResultList();
	}

}
