package com.laboratorio.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import com.laboratorio.entity.Laboratorio;

/**
 * Servlet implementation class DbDeleteServlet
 */
@Transactional
@WebServlet("/private/DbDeleteServlet")
public class DbDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager em;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DbDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		Laboratorio lab = em.find(Laboratorio.class, id);
		em.remove(lab);
		request.getRequestDispatcher("DbListServlet").forward(request, response);
	}

}
