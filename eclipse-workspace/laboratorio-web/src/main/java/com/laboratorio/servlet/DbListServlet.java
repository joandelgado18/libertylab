package com.laboratorio.servlet;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.laboratorio.entity.Laboratorio;
import com.laboratorio.sb.DBFacade;

/**
 * Servlet implementation class DbListServlet
 */
@WebServlet("/private/DbListServlet")
public class DbListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Inject
	DBFacade dbf;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DbListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Laboratorio> mensajes = dbf.listarMensajesLaboratorio();
		request.setAttribute("mensajes", mensajes);
		request.getRequestDispatcher("listardb.jsp").forward(request, response);
	}

}
