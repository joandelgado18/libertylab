package com.laboratorio.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.laboratorio.sb.MensajeColaFacade;

/**
 * Servlet implementation class MqServlet
 */
@WebServlet("/private/MqServlet")
public class MqServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@Inject
	MensajeColaFacade mcf;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MqServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mensaje = request.getParameter("mensaje");
		try {
			mcf.escribirMensajeCola(mensaje);
			request.setAttribute("resultado", "ok");
			request.getRequestDispatcher("enviarmq.jsp").forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
